-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2018 at 10:36 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `examples`
--
CREATE DATABASE IF NOT EXISTS `examples` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `examples`;

-- --------------------------------------------------------

--
-- Table structure for table `myguests`
--

DROP TABLE IF EXISTS `myguests`;
CREATE TABLE `myguests` (
  `IdUser` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `myguests`
--

INSERT INTO `myguests` (`IdUser`, `firstname`, `lastname`, `email`) VALUES
(1, 'New Name', 'Doe', 'john@example.com'),
(3, 'Julie', 'Dooley', 'julie@example.com'),
(4, 'John', 'Doe', 'john@example.com'),
(6, 'Julie', 'Dooley', 'julie@example.com'),
(7, 'John', 'Doe', 'john@example.com'),
(9, 'Julie', 'Dooley', 'julie@example.com'),
(10, 'John', 'Doe', 'john@example.com'),
(12, 'Julie', 'Dooley', 'julie@example.com'),
(13, 'John', 'Doe', 'john@example.com'),
(15, 'Julie', 'Dooley', 'julie@example.com'),
(16, 'John', 'Doe', 'john@example.com'),
(18, 'Julie', 'Dooley', 'julie@example.com'),
(19, 'John', 'Doe', 'john@example.com'),
(21, 'Julie', 'Dooley', 'julie@example.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `myguests`
--
ALTER TABLE `myguests`
  ADD PRIMARY KEY (`IdUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `myguests`
--
ALTER TABLE `myguests`
  MODIFY `IdUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
