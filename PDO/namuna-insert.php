<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "examples";
    
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Ba BM payvast shud.";
        
        // prepare sql and bind parameters
        $stmt = $conn->prepare("INSERT INTO MyGuests (firstname, lastname, email) 
        VALUES (:firstname, :lastname, :email)");

        $stmt->bindParam(':firstname', $firstname);
        $stmt->bindParam(':lastname', $lastname);
        $stmt->bindParam(':email', $email);

        
        // insert a row
        $firstname = "Test1";
        $lastname = "Tester1";
        $email = "test@example.com";
        $stmt->execute();

       
        // insert another row
        $firstname = "Mary 1";
        $lastname = "Moe 1";
        $email = "mary@example.com";
        $stmt->execute();
    
        //var_dump($natija);
        /*
        // insert another row
        $firstname = "Julie";
        $lastname = "Dooley";
        $email = "julie@example.com";
         $a= $stmt->execute();
        var_dump($a);
        echo "New records created successfully " . $conn->lastInsertId();
        */
        }
    catch(PDOException $e)
        {
        echo "Error: " . $e->getMessage();
        }
    $conn = null;

?>