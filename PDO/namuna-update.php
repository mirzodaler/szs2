<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "examples";
    
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "UPDATE MyGuests SET firstname = ? WHERE idUser = ?";

        $id=1;
        $name= "Tester";
        $natija = $conn->prepare($sql)->execute([$name, $id]);

        var_dump($natija);

        }
    catch(PDOException $e)
        {
        echo "Error: " . $e->getMessage();
        }

    unset($conn);

?>