<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "examples";
    
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
/* Other forms
    $stmt = $pdo->prepare('SELECT * FROM users WHERE email = ? AND status=?');
    $stmt->execute([$email, $status]);
    $user = $stmt->fetch();
    // or
    $stmt = $pdo->prepare('SELECT * FROM users WHERE email = :email AND status=:status');
    $stmt->execute(['email' => $email, 'status' => $status]);
    $user = $stmt->fetch();
*/

        $stmt = $conn->query('SELECT * FROM MyGuests');

        while ($row = $stmt->fetchObject())     //you can use fetch, fetchAll, fetchColumn, fetchObject
        {

            //echo $row["firstname"];
            //echo "<br/>";
            //var_dump($row);
            echo $row->email;
            echo "<br/>";
        }

        }
    catch(PDOException $e)
        {
        echo "Error: " . $e->getMessage();
        }

    unset($conn);

?>