<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "examples";
    
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $name= "Mary";
        $stmt = $conn->prepare("DELETE FROM MyGuests WHERE firstname = ?");
        $natija = $stmt->execute([$name]);
        
        echo $deleted = $stmt->rowCount();

        var_dump($natija);

        }
    catch(PDOException $e)
        {
        echo "Error: " . $e->getMessage();
        }

    unset($conn);

?>