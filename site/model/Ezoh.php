<?php
    class Ezoh{
        public $idEzoh;
        public $nom;
        public $email;
        public $ezoh;
        public $sana;

        public function sabtiEzoh(){
            $connection = BazaiMalumot::getInstance();
            $stmt = $connection->prepare('INSERT INTO ezohho SET 
            nom=:nom, 
            email = :email, 
            ezoh=:ezoh, 
            sana=:sana');
    
            $stmt->bindParam(':nom', $this->nom);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':ezoh', $this->ezoh);
            $stmt->bindParam(':sana', $this->sana);
            $stmt->execute();
            
            return  $connection->lastInsertId();
        }

        public static function giriftaniEzohho(){
            $connection = BazaiMalumot::getInstance();
            $stmt = $connection->prepare('SELECT * FROM ezohho');
            $stmt->execute();
            $natija = array();
            while($ezoh = $stmt->fetchObject()){
                $natija[] = $ezoh;
            }
            return $natija;
        }
    }

?>