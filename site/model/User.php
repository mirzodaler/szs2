<?php

class User{
    private $idUser;
    private $nom;
    private $nasab;
    private $nomiPadar;
    private $jins;
    private $email;
    private $parol;
    private $sanaiQayd;
    private $sanaVoridotiOkhirin;

    public function __construct(){

    }

    public function getFullName(){
        return $this->nom." ".$this->nasab;
    }

    public static function getUserById($userId){
        $connection = BazaiMalumot::getInstance();
        $stmt = $connection->prepare('SELECT * FROM users WHERE idUser = :id');
        $stmt->execute(['id' => $userId]);
        $result = $stmt->fetchObject();
        if(!$result){
            //inkhel user bo in ID mavjud nest
            return null;
        }else{
            return self::loadUserByFetchObject($result);
        }
    }

    public static function getUserByEmail($userEmail){
        $connection = BazaiMalumot::getInstance();
        $stmt = $connection->prepare('SELECT * FROM users WHERE email = :email');
        $stmt->execute(['email' => $userEmail]);
        $result = $stmt->fetchObject();
        if(!$result){
            //inkhel user bo in email mavjud nest
            return null;
        }else{
            return self::loadUserByFetchObject($result);
        }
    }

    private static function loadUserByFetchObject($fetchObject){
                $user=new User();
                $user->setIdUser($fetchObject->idUser);
                $user->setNom($fetchObject->nom);
                $user->setNasab($fetchObject->nasab);
                $user->setNomiPadar($fetchObject->nomiPadar);
                $user->setJins($fetchObject->jins);
                $user->setEmail($fetchObject->email);
                $user->setSanaiQayd($fetchObject->sanaiQayd);
                $user->setSanaVoridotiOkhirin($fetchObject->sanaiVoridotiOkhirin);
                return $user;
    }

    public static function login($email, $parol){
        $connection = BazaiMalumot::getInstance();
        $stmt = $connection->prepare('SELECT * FROM users WHERE email = :email');
        $stmt->execute(['email' => $email]);
        $result = $stmt->fetchObject();
        if(!$result){
            //inkhel user bo in email mavjud nest
            return array("error" => "Chunin login mavjud nest");
        }else{
            if($result->parol == md5($parol)){
                //login va parol durust ast
                return array("user" => self::loadUserByFetchObject($result));
            }else{
                //parol khato vorid karda shud
                return array("error" => "Parol khato vorid karda shud");
            }
        }
    }

    /**
     * Get the value of idUser
     */ 
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * Get the value of nasab
     */ 
    public function getNasab()
    {
        return $this->nasab;
    }

    /**
     * Set the value of nasab
     *
     * @return  self
     */ 
    public function setNasab($nasab)
    {
        $this->nasab = $nasab;
        return $this;
    }

    /**
     * Get the value of nomiPadar
     */ 
    public function getNomiPadar()
    {
        return $this->nomiPadar;
    }

    /**
     * Set the value of nomiPadar
     *
     * @return  self
     */ 
    public function setNomiPadar($nomiPadar)
    {
        $this->nomiPadar = $nomiPadar;
        return $this;
    }

    /**
     * Get the value of jins
     */ 
    public function getJins()
    {
        return $this->jins;
    }

    /**
     * Set the value of jins
     *
     * @return  self
     */ 
    public function setJins($jins)
    {
        $this->jins = $jins;
        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get the value of parol
     */ 
    public function getParol()
    {
        return $this->parol;
    }

    /**
     * Set the value of parol
     *
     * @return  self
     */ 
    public function setParol($parol)
    {
        $this->parol = md5($parol);
        return $this;
    }

    /**
     * Get the value of sanaiQayd
     */ 
    public function getSanaiQayd()
    {
        return $this->sanaiQayd;
    }

    /**
     * Set the value of sanaiQayd
     *
     * @return  self
     */ 
    public function setSanaiQayd($sanaiQayd)
    {
        $this->sanaiQayd = $sanaiQayd;
        return $this;
    }

    /**
     * Get the value of sanaVoridotiOkhirin
     */ 
    public function getSanaVoridotiOkhirin()
    {
        return $this->sanaVoridotiOkhirin;
    }

    /**
     * Set the value of idUser
     *
     * @return  self
     */ 
    public function setIdUser($idUser)
    {
        $this->idUser = (int)$idUser;
        return $this;
    }

    /**
     * Set the value of sanaVoridotiOkhirin
     *
     * @return  self
     */ 
    public function setSanaVoridotiOkhirin($sanaVoridotiOkhirin)
    {
        $this->sanaVoridotiOkhirin = $sanaVoridotiOkhirin;
        return $this;
    }
}
?>