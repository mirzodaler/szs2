<?php

class BazaiMalumot{

    /**
     * Khosiyathoi shakhsii statici baroi nigoh doshtani malumothoi zaruri baroi payvastagi ba BM
     */
    private static $servername="localhost";
    private static $username="root";
    private static $password="";
    private static $dbname="szs2";

    //Khosiyati shakhsii statici baroi nigoh doshtani payvastagii yagona ba bazai malumot
    private static $instance;

    //Konstruktori shakhsi baroi sokhta natavonistani instanci class
    private function __construct(){
    }

    /**
     * Method baroi giriftani/sokhtani payvastagi ba bazai malumotho
     */
    public static function getInstance(){
        if(self::$instance!=null){
            return self::$instance;
        }else{
            try{
                self::$instance = new PDO("mysql:host=".self::$servername.";dbname=".self::$dbname, self::$username, self::$password);
                // set the PDO error mode to exception
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return self::$instance;
            }
            catch(PDOException $e)
            {
                self::$instance=null;
                throw new Exception("Khatogi hangomi payvastshavi ba bazai malumot. ".$e->getMessage(), 0, $e);
            }
        }
    }
}

?>