<?php

//method baroi ba tavri avtomatiki include kardani classhoi istifodaburdashuda dar fayli jori
spl_autoload_register(function ($class_name) {
    require_once $class_name . '.php';
});

// Sokhtani istifodabarandai nav
$newUser=new User();
var_dump($newUser);

//avtorizatsiyai istifodabaranda: Logini khato
echo "<hr/>Avtorizatsiya: Logini khato";
$user = User::login("mirzodaler1@gmail.com", "farqash nest");
var_dump($user);

//avtorizatsiyai istifodabaranda: Logini durust va paroli khato
echo "<hr/>Avtorizatsiya: Logini durust va paroli khato";
$user = User::login("mirzodaler@gmail.com", "khato");
var_dump($user);

//avtorizatsiyai istifodabaranda: Login va paroli durust
echo "<hr/>Avtorizatsiya: Login va paroli durust";
$user = User::login("mirzodaler@gmail.com", "test");
var_dump($user);

//Giriftani istifodabaranda bo vositai Email
echo "<hr/>Giriftani istifodabaranda bo vositai Email";
$user = User::getUserByEmail("mirzodaler@gmail.com");
var_dump($user);

//Giriftani istifodabaranda bo vositai ID
echo "<hr/>Giriftani istifodabaranda bo vositai ID";
$user = User::getUserById(1);
var_dump($user);

?>