-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2018 at 07:29 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `szs2`
--

-- --------------------------------------------------------

--
-- Table structure for table `ezohho`
--

CREATE TABLE `ezohho` (
  `idEzoh` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `ezoh` text NOT NULL,
  `sana` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ezohho`
--

INSERT INTO `ezohho` (`idEzoh`, `nom`, `email`, `ezoh`, `sana`) VALUES
(1, 'Tester', 'test@test.com', 'in ezoh ast', '2018-04-20 12:52:00'),
(2, 'Tester123', 'test@test.com123', '123123', '2018-04-20 13:13:58'),
(3, 'Tester', 'test@test.com', '', '2018-04-20 13:15:02'),
(4, 'Tester', 'test@test.com', '123123', '2018-04-20 13:16:31'),
(5, '123', 'test@test.com', '1123', '2018-04-20 13:17:17'),
(6, '123', 'test@test.com', '1123', '2018-04-20 13:21:16'),
(7, 'Namuna', 'test@test.com', 'Namunai ezoh', '2018-04-20 13:29:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `nasab` varchar(50) NOT NULL,
  `nomiPadar` varchar(50) NOT NULL,
  `jins` tinyint(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `parol` varchar(32) NOT NULL COMMENT 'MD5',
  `sanaiQayd` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sanaiVoridotiOkhirin` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUser`, `nom`, `nasab`, `nomiPadar`, `jins`, `email`, `parol`, `sanaiQayd`, `sanaiVoridotiOkhirin`) VALUES
(1, 'Mirzodaler', 'Muhsinzoda', 'Yodgori', 1, 'mirzodaler@gmail.com', '098f6bcd4621d373cade4e832627b4f6', '2018-03-01 13:02:57', '2018-03-01 13:02:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ezohho`
--
ALTER TABLE `ezohho`
  ADD PRIMARY KEY (`idEzoh`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `username` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ezohho`
--
ALTER TABLE `ezohho`
  MODIFY `idEzoh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
