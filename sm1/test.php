<?php

//method baroi ba tavri avtomatiki include kardani classhoi istifodaburdashuda dar fayli jori
spl_autoload_register(function ($class_name) {
    require_once $class_name . '.php';
});

    $mizoj = new Mizoj();
    $mizoj->nom = "Client 1";

    $m1=new Mahsulot();
    $m1->nom = "Non";
    $m1->narkh = 5;

    $m2=new Mahsulot();
    $m2->nom = "Sok";
    $m2->narkh = 11;

    $kharid = new Kharid();
    $kharid->sana = "16.03.2018";
    $kharid->mizoj = $mizoj;

    $tarkibiKharid1 = new TarkibiKharid();
    $tarkibiKharid1->miqdor = 2;
    $tarkibiKharid1->kharid = $kharid;
    $tarkibiKharid1->mahsulot = $m1;
    //echo $tarkibiKharid1->hisobiUmumi();        //boyad 2*5=10

    //echo "<br/>";

    $tarkibiKharid2 = new TarkibiKharid();
    $tarkibiKharid2->miqdor = 1;
    $tarkibiKharid2->kharid = $kharid;
    $tarkibiKharid2->mahsulot = $m2;
    //echo $tarkibiKharid2->hisobiUmumi();        //boyad 1*11=11

    $kharid->tarkibiKharidho = array($tarkibiKharid1, $tarkibiKharid2);

    //var_dump($kharid);

    echo "<pre>";
    //print_r($kharid);
    echo "</pre>";

    echo "Mizoj: ".$mizoj->nom;
    echo "<br/>Sana: ".$kharid->sana;
?>
    <table border=1>
        <th>#</th>
        <th>Mahsulot</th>
        <th>Miqdor</th>
        <th>Umumi</th>
        <?php
            $i=1;
            foreach($kharid->tarkibiKharidho as $tarkib){
                echo "<tr>";
                echo "<td>".$i++."</td>";
                echo "<td>".$tarkib->mahsulot->nom."</td>";
                echo "<td align=center>".$tarkib->miqdor."</td>";
                echo "<td align=center>".$tarkib->hisobiUmumi()."</td>";
                echo "</tr>";
            }
        ?>
    </table>
    <?php
            echo "<br/> Hisobi mablagi umumi: ";
            echo $kharid->hisobiMablagiUmumi();
    ?>